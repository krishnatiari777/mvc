﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using ProductWebApplication.MVC.Repository;

namespace MVCUserAppTest
{
    
    public class UserRepositoryTest:IClassFixture<UserDbFixture>
    {
        readonly UserRepository _userRepository;
        public UserRepositoryTest(UserDbFixture userDbFixture)
        {
            _userRepository = new UserRepository(userDbFixture._productDbContext);
        }
        [Fact]

        public void AddUserReturnSuccess()
        {
            //Initialization
            string ExpectedUserName = "User2";
            //Call Devlopment code
            _userRepository.AddUser(new ProductWebApplication.MVC.Models.User() { Id = 2, Name = "User2", Password = "User2", Location = "Indore" });
            var ActualUserName = _userRepository.GetAllUsers()[1].Name;
            //assert
            Assert.Equal(ExpectedUserName, ActualUserName);

        }
    }
}
